import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import { Field, reduxForm } from "redux-form";
import CardContent from "@material-ui/core/CardContent";
import MaterialTextField from "./MaterialTextField";

const validate = values => {
  const errors = {};
  if (!values.password) {
    errors.password = "Required";
  } else if (values.password.length > 15) {
    errors.password = "Must be 15 characters or less";
  }
  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }
  return errors;
};

const styles = theme => ({
  button: {
    marginTop: 14
  },
  card: {
    marginTop: 24,
    color: theme.background
  },
  textField: {
    marginBottom: 24
  }
});

class TextFields extends React.Component {
  render() {
    const { classes, handleSubmit } = this.props;

    return (
      <Card className={classes.card}>
        <CardContent>
          <h2>Conexión al sistema</h2>
          <form className={classes.container} onSubmit={handleSubmit}>
            <Field
              label="Email"
              name="email"
              className={classes.textField}
              component={MaterialTextField}
            />
            <Field
              className={classes.textField}
              type="password"
              label="Contraseña"
              name="password"
              component={MaterialTextField}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.button}
            >
              Ingresar
            </Button>
          </form>
        </CardContent>
      </Card>
    );
  }
}

const ContactForm = reduxForm({
  // a unique name for the form
  form: "login",
  validate
})(TextFields);

const comp = withStyles(styles)(ContactForm);

export default comp;
