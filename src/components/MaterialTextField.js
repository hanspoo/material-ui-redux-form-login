/* eslint react/forbid-prop-types: 0 */

import TextField from "@material-ui/core/TextField";
import PropTypes from "prop-types";
import React from "react";

const MaterialTextField = ({ input, meta: { touched, error }, ...props }) => {
  const hasError = Boolean(touched && error);
  return <TextField error={hasError} {...input} {...props} fullWidth />;
};

MaterialTextField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired
};

export default MaterialTextField;
