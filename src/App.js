import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import LoginForm from "./components/LoginForm";

export default class App extends React.Component {
  conectando = e => {
    console.log(e);
  };
  render() {
    return (
      <div style={styles.container}>
        <AppBar>
          <Toolbar>HOSPITAL ONLINE</Toolbar>
        </AppBar>

        <LoginForm onSubmit={this.conectando} />
      </div>
    );
  }
}

const styles = {
  container: {
    paddingTop: 64,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
};
